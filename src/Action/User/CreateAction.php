<?php

declare(strict_types=1);

namespace App\Action\User;

use App\Entity\User;
use App\Form\UserType;
use App\Library\EmailChecker;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface as FormFactory;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CreateAction
{
    protected $manager;
    protected $formFactory;
    protected $requestStack;
    protected $emailChecker;
    protected $session;

    public function __construct(
        EntityManagerInterface $manager,
        FormFactory $formFactory,
        RequestStack $requestStack,
        EmailChecker $emailChecker,
        SessionInterface $session
    ) {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
        $this->requestStack = $requestStack;
        $this->emailChecker = $emailChecker;
        $this->session = $session;
    }

    public function __invoke(): ?array
    {
        $user = new User();

        $request = $this->requestStack->getCurrentRequest();

        $form = $this->formFactory->create(UserType::class, $user);

        if ($request !== null && $request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $user = $form->getData();

                if (!$this->emailChecker->isDisposable($user->getEmail())) {
                    $this->manager->persist($user);
                    $this->manager->flush();

                    $this->session->getFlashBag()->add('success', 'New user added successfully!');

                    return null;
                }
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }
}
