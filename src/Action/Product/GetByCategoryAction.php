<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepository;

class GetByCategoryAction
{
    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(string $name): array
    {
        if ($name === 'all') {
            return [
                'products' => $this->repository->findBy([], ['name' => 'ASC']),
                'category_name' => $name
            ];
        }

        return [
            'products' => $this->repository->findByCategory($name),
            'category_name' => $name
        ];
    }
}
