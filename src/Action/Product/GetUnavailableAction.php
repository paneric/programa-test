<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepository;

class GetUnavailableAction
{
    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(): array
    {
        return [
            'products' => $this->repository->findBy(
                ['stock' => 0 ]
            )
        ];
    }
}
