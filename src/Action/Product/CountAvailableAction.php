<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepository;

class CountAvailableAction
{
    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(): array
    {
        return [
            'amount' => $this->repository->count(['stock' => 1])
        ];
    }
}
