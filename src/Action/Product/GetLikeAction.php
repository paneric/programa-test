<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepository;

class GetLikeAction
{
    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(string $sub): array
    {
        if ($sub === 'all') {
            return [
                'products' => $this->repository->findAll()
            ];
        }

        return [
            'products' => $this->repository->findLike('%' . $sub . '%')
        ];
    }
}
