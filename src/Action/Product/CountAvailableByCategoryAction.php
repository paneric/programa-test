<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepository;

class CountAvailableByCategoryAction
{
    protected $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(string $name): array
    {
        if ($name === 'all') {
            $products = $this->repository->findBy(['stock' => 1]);

            return [
                'amount' => count($products),
                'category_name' => $name
            ];
        }

        $products = $this->repository->findByCategoryAndStock($name, 1);

        return [
            'amount' => count($products),
            'category_name' => $name
        ];
    }
}
