<?php

declare(strict_types=1);

namespace App\Library;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class EmailChecker
{
    protected $httpClient;
    protected $session;

    public function __construct(HttpClientInterface $httpClient, SessionInterface $session)
    {
        $this->httpClient = $httpClient;
        $this->session = $session;
    }

    public function isDisposable(string $email): bool
    {
        try {
            $response = $this->httpClient->request(
                'GET',
                'https://disposable.debounce.io/?email=' . $email
            );

            $content = $response->toArray();
        } catch (
            ClientExceptionInterface |
            DecodingExceptionInterface |
            RedirectionExceptionInterface |
            ServerExceptionInterface |
            TransportExceptionInterface $e
        ) {
            $this->session->getFlashBag()->add('error', 'Disposable email address verification error!');

            return true;
        }

        if ($content['disposable'] === 'true') {
            $this->session->getFlashBag()->add('error', 'Disposable email address!');

            return true;
        }

        return false;
    }
}
