<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Parameter;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findLike(string $sub): array
    {
        return $this->createQueryBuilder('p')
            ->where('p.name LIKE :name')
            ->setParameter('name', $sub)
            ->getQuery()
            ->getResult();
    }

    public function findByCategory(string $name): array
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.categories', 'c')
            ->where('c.name = :name')
            ->orderBy('p.name', 'ASC')
            ->setParameters(new ArrayCollection([
                new Parameter('name', $name),
            ]))
            ->getQuery()
            ->getResult();
    }

    public function findByCategoryAndStock(string $name, int $stock): array
    {
        return $this->createQueryBuilder('p')
            ->innerJoin('p.categories', 'c')
            ->where('c.name = :name')
            ->andWhere('p.stock = :stock')
            ->orderBy('p.name', 'ASC')
            ->setParameters(new ArrayCollection([
                new Parameter('name', $name),
                new Parameter('stock', $stock),
            ]))
            ->getQuery()
            ->getResult();
    }
}
