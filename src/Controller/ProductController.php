<?php

declare(strict_types=1);

namespace App\Controller;

use App\Action\Product\CountAvailableByCategoryAction;
use App\Action\Product\GetByCategoryAction;
use App\Action\Product\GetUnavailableAction;
use App\Action\Product\GetLikeAction;
use App\Action\Product\CountAvailableAction;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends AbstractController
{
    public function showAvailableAmount(
        CountAvailableAction $action
    ): Response {
        return $this->render(
            'product/show.html.twig',
            $action()
        );
    }

    public function showUnavailable(
        GetUnavailableAction $action
    ): Response {
        return $this->render(
            'product/show.html.twig',
            $action()
        );
    }

    public function showLike(
        GetLikeAction $action,
        string $sub
    ): Response {
        return $this->render(
            'product/show.html.twig',
            $action($sub)
        );
    }

    public function showAvailableByCategoryAmount(
        CountAvailableByCategoryAction $action,
        string $name
    ): Response {
        return $this->render(
            'product/show_by_category.html.twig',
            $action($name)
        );
    }

    public function showByCategory(
        GetByCategoryAction $action,
        string $name
    ): Response {
        return $this->render(
            'product/show_by_category.html.twig',
            $action($name)
        );
    }
}
