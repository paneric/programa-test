<?php

declare(strict_types=1);

namespace App\Controller;

use App\Action\User\CreateAction;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class UserController extends AbstractController
{
    public function index(): Response
    {
        return $this->render(
            'user/index.html.twig'
        );
    }

    public function add(CreateAction $action): Response
    {
        $result = $action();
        if ($result === null) {
            return $this->redirect('/user');
        }

        return $this->render(
            'user/add.html.twig',
            $result
        );
    }
}
