# Programa Test

Junior PHP Developer code challenge

## Development Environment

* **Linux Ubuntu 18.04.5**
* **MySQL 5.7.34**
* **PHP 7.3.28**
* **Symfony 5.2.9**

## Installation

```sh
$ git clone https://paneric@bitbucket.org/paneric/programa-test.git
$ cd programa-test
$ composer install
```

Adapt database credentials. `*.env*` is exceptionally delivered within repository.
```dotenv
DATABASE_URL="mysql://root:root@127.0.0.1:3306/programa_test?serverVersion=5.7"
```

```shell
$ bin/console doctrine:database:create
$ bin/console doctrine:migrations:migrate
$ bin/console hautelook:fixtures:load
$ symfony serve
```

## Task #3

### Routes

`http://localhost:8000/user` - User index page  
`http://localhost:8000/user/add` - User add page


## Task #4

### Routes (1)

`http://localhost:8000/products/show-available-amount` - Amount of available products  
`http://localhost:8000/products/show-unavailable` - Collection of unavailable products  
`http://localhost:8000/products/show-like/{sub}` - Collection by a partial name (sub - expected phrase)

### Routes (2)

`http://localhost:8000/products/show-available-by-category-amount/{name}` - Amount of available products by category name  
`http://localhost:8000/products/show-by-category/{name}` - Collection of products by category name (ascending order)



## Task #5

### Tested file:

*src/Action/User/CreateAction.php*
```PHP
<?php

declare(strict_types=1);

namespace App\Action\User;

use App\Entity\User;
use App\Form\UserType;
use App\Library\EmailChecker;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface as FormFactory;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CreateAction
{
    protected $manager;
    protected $formFactory;
    protected $requestStack;
    protected $emailChecker;
    protected $session;

    public function __construct(
        EntityManagerInterface $manager,
        FormFactory $formFactory,
        RequestStack $requestStack,
        EmailChecker $emailChecker,
        SessionInterface $session
    ) {
        $this->manager = $manager;
        $this->formFactory = $formFactory;
        $this->requestStack = $requestStack;
        $this->emailChecker = $emailChecker;
        $this->session = $session;
    }

    public function __invoke(): ?array
    {
        $user = new User();

        $request = $this->requestStack->getCurrentRequest();

        $form = $this->formFactory->create(UserType::class, $user);

        if ($request !== null && $request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $user = $form->getData();

                if (!$this->emailChecker->isDisposable($user->getEmail())) {
                    $this->manager->persist($user);
                    $this->manager->flush();

                    $this->session->getFlashBag()->add('success', 'New user added successfully!');

                    return null;
                }
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }
}
```

### Run phpSpec tests:

```shell
$ bin/phpspec run
```
